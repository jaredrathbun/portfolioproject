## Let Language Grammar Rules

expr &rarr; **let** id **:=** expr **in** expr | term { ( **+** | **-** ) term }
| **hd** expr | **tl** expr

exprLst &rarr; **list(** [ expr {, expr} ] **)** | id

term &rarr; factor { ( <strong>*</strong> | **/**  | **++** ) factor }

factor &rarr; id | num | **(** expr **)** | **list(** [ (id | num ) {, (id | num)} ]**)**
