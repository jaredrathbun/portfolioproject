package ast.nodes;

import lexer.TokenType;
import environment.Environment;
import java.util.LinkedList;

/**
 * This node represents the program non-terminal.
 * 
 * @author Zach Kissel
 * @author Jared Rathbun
 * @author Bernardo Santos
 */
public class BinOpNode extends SyntaxNode {
  private TokenType op;
  private SyntaxNode leftTerm;
  private SyntaxNode rightTerm;

  /**
   * Constructs a new binary operation syntax node.
   * 
   * @param lterm the left operand.
   * @param op    the binary operation to perform.
   * @param rterm the right operand.
   */
  public BinOpNode(SyntaxNode lterm, TokenType op, SyntaxNode rterm) {
    this.op = op;
    this.leftTerm = lterm;
    this.rightTerm = rterm;
  }

  /**
   * Evaluate the node.
   * 
   * @param env the executional environment we should evaluate the
   *            node under.
   * @return the object representing the result of the evaluation.
   */
  public Object evaluate(Environment env) {
    
    boolean useDouble = false;

    Object lval, rval;

    // If the operation is not a concatenation, get the "primitives" for both 
    // sides.
    if (op != TokenType.CONCAT) {
      lval = leftTerm.evaluate(env);
      rval = rightTerm.evaluate(env);
      
      if (lval instanceof TokenNode) {
        lval = ((TokenNode) lval).evaluate(env);
      }
      if (rval instanceof TokenNode) {
        rval = ((TokenNode) rval).evaluate(env);
      }
    } else {
      lval = leftTerm;
      rval = rightTerm;
    }

    // If either value is null, we can't proceed.
    if (lval == null || rval == null)
      return null;

    // // Make sure the type is sound.
    if (!(lval instanceof Integer || lval instanceof Double || 
        lval instanceof ListNode || lval instanceof HeadNode || 
        lval instanceof TailNode || lval instanceof LetNode || 
        lval instanceof TokenNode) && !(rval instanceof Double || 
        rval instanceof Integer || rval instanceof ListNode || 
        rval instanceof HeadNode || rval instanceof TailNode ||
        rval instanceof LetNode || rval instanceof TokenNode)) {
        return null;
    }

    if (lval.getClass() != rval.getClass()) {
      System.out.println("Error: mixed type expression.");
      return null;
    }

    if (lval instanceof Double)
      useDouble = true;

    // Perform the operation base on the type.
    switch (op) {
      case ADD:
        if (useDouble) {
          return (Double) lval + (Double) rval;
        } else {
          return (Integer) lval + (Integer) rval;
        }
      case SUB:
        if (useDouble) {
          return (Double) lval - (Double) rval;
        } else {
          return (Integer) lval - (Integer) rval;
        }
      case MULT:
        if (useDouble) {
          return (Double) lval * (Double) rval;
        } else {
          return (Integer) lval * (Integer) rval;
        }
      case DIV:
        if (useDouble) {
          return (Double) lval / (Double) rval;
        } else {
          return (Integer) lval / (Integer) rval;
        }
      case CONCAT:
        // Create the return list.
        LinkedList<TokenNode> returnList = new LinkedList<>();

        // Add all of the left list to the return list, then the right.
        returnList.addAll(((ListNode) lval).getList());
        returnList.addAll(((ListNode) rval).getList());

        // Finally, return the returnList.
        return new ListNode(returnList).evaluate(env);
      default:
        return null;
    }
  }
}
