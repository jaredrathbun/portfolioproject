package ast.nodes;

import lexer.Token;
import environment.Environment;

/**
 * This node represents a let expression.
 * 
 * @author Zach Kissel
 */
public class LetNode extends SyntaxNode {
  private Token var;
  private SyntaxNode varExpr;
  private SyntaxNode expr;

  /**
   * Constructs a new binary operation syntax node.
   * 
   * @param var the variable identifier.
   * @param varExpr the expression that give the variable value.
   * @param expr the expression that uses the variables value.
   */
  public LetNode(Token var, SyntaxNode varExpr, SyntaxNode expr) {
    this.var = var;
    this.varExpr = varExpr;
    this.expr = expr;
  }

  /**
   * Evaluate the node.
   * 
   * @param env the executional environment we should evaluate the node under.
   * @return the object representing the result of the evaluation.
   */
  public Object evaluate(Environment env) {
    // Update the environment with the ID and its associated value.
    env.updateEnvironment(var, varExpr);

    // Evaluate the expression.
    return expr.evaluate(env);
  }
}
