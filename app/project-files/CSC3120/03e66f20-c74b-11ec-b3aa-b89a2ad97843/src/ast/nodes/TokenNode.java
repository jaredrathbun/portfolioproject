package ast.nodes;

import lexer.TokenType;
import lexer.Token;
import environment.Environment;

/**
 * This node represents the a token in the grammar.
 * 
 * @author Zach Kissel
 */
public class TokenNode extends SyntaxNode {
  private Token token; // The token type.

  /**
   * Constructs a new token node.
   * 
   * @param token the token to associate with the node.
   */
  public TokenNode(Token token) {
    this.token = token;
  }

  /**
   * Evaluate the node.
   * 
   * @param env the executional environment we should evaluate the node under.
   * @return the object representing the result of the evaluation.
   */
  public Object evaluate(Environment env) {
    switch (token.getType()) {
      case ID:
        return env.lookup(token);
      case INT:
        return Integer.valueOf(token.getValue());
      case REAL:
        return Double.valueOf(token.getValue());
      default:
        return token;
    }
  }

  /**
   * !DEBUG METHOD!
   * Allows to see the type of the current token.
   * 
   * @return A TokenType representing the type of this token.
   */
  public TokenType getType() {
    return token.getType();
  }

  /**
   * !DEBUG METHOD!
   * Allows to see the current token.
   * 
   * @return The token.
   */
  public Token getToken() {
    return this.token;
  }
}