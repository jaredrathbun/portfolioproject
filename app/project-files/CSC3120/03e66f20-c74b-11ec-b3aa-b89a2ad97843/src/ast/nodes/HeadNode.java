package ast.nodes;

import environment.Environment;

/**
 * Representation of a hd operation.
 * 
 * @author Jared Rathbun
 * @author Bernardo Santos
 */
public class HeadNode extends SyntaxNode {

    /**
     * The node that the head operation is going to take place on.
     */
    private SyntaxNode node;

    /**
     * Constructor.
     * 
     * @param syntaxNode The node to perform the head operation on.
     */
    public HeadNode(SyntaxNode syntaxNode) {
        this.node = syntaxNode;
    }

    /**
     * Returns just the head of the {@code node}.
     * 
     * @param env The environment to evaluate under.
     * @return An object representing the result of the evaluation.
     */
    public Object evaluate(Environment env) {
        if (node == null) {
            System.out.println("Empty list!");
            return null;
        } else {
            if (node instanceof ListNode) {
                return ((ListNode) node).getHead().evaluate(env);
            } else {
                Object evalRes = node.evaluate(env);

                if (evalRes == null) {
                    System.out.println("Undefined variable " + 
                        ((TokenNode) node).getToken().getValue() + ".");
                    return null;
                }
                
                return ((ListNode) node.evaluate(env)).getHead().evaluate(env);
            }  
        }
    }

}
