package ast.nodes;

import environment.Environment;

/**
 * Representation of a tl operation.
 * 
 * @author Jared Rathbun
 * @author Bernardo Santos
 */
public class TailNode extends SyntaxNode {

    /**
     * The node that the tail operation is going to take place on.
     */
    private SyntaxNode node;

    /**
     * Constructor.
     * 
     * @param syntaxNode The node to perform the tail operation on.
     */
    public TailNode(SyntaxNode syntaxNode){
        this.node = syntaxNode;
    }

    /**
     * Returns just the tail of the {@code node}.
     * 
     * @param env The environment to evaluate under.
     * @return An object representing the result of the evaluation.
     */
    public Object evaluate(Environment env){
        if (node == null) {
            System.out.println("Can't find tail of list.");
            return null;
        } else {
            if (node instanceof ListNode) {
                ListNode list = (ListNode) node;
                if (list.getListSize() < 2) {
                    System.out.println("Can't find tail of list.");
                    return null;
                } else {
                    return list.getTail().evaluate(env);
                }
            } else {
                return ((ListNode) node.evaluate(env)).getTail();
            }
        }
    }

}
