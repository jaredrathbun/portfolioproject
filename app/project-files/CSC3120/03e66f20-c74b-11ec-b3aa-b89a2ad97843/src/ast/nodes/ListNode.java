package ast.nodes;

import environment.Environment;
import lexer.TokenType;

import java.util.LinkedList;

/**
 * Representation of a List type.
 * 
 * @author Jared Rathbun
 * @author Bernardo Santos
 */
public class ListNode extends SyntaxNode {

  private LinkedList<TokenNode> tokenList;

  /**
   * Constructor which initializes the ListNode.
   *
   * @param tokenList A List of INT or REAL values.
   */
  public ListNode(LinkedList<TokenNode> tokenList) {
    this.tokenList = tokenList;
  }

  /**
   * Returns the head of the list.
   * 
   * @return The head of the list, null if it does not exist.
   */
  public TokenNode getHead() {
    if (tokenList.size() > 0) {
      return tokenList.get(0);
    }

    return null;
  }

  /**
   * Return the tail of the list.
   * 
   * @return The tail of the list in a ListNode, null if the tail cannot be 
   * found.
   */
  public ListNode getTail() {
    if (tokenList.size() > 0) {
      if (tokenList.size() == 1) {
        return null;
      } else {

        LinkedList<TokenNode> newList = new LinkedList<>();

        if (this.tokenList.size() == 1) {
          newList.add(this.tokenList.get(0));
        } else {

          for (int i = 1; i < this.tokenList.size(); i++) {
            newList.add(this.tokenList.get(i));
          }
        }

        return new ListNode(newList);
      }
    }
    return null;
  }

  /**
   * Gets the size of the list.
   * 
   * @return An integer representing the size of the list.
   */
  public int getListSize() {
    return this.tokenList.size();
  }

  /**
   * Returns the current list of tokens in a LinkedList object.
   * 
   * @return A LinkedList of TokenNodes.
   */
  public LinkedList<TokenNode> getList() {
    return this.tokenList;
  }

  /**
   * Implementation for evaluating a ListNode.
   *
   * @param env The environment to evaluate the list under.
   * @return An object representing the result of the evaluation.
   */
  @Override
  public Object evaluate(Environment env) {
    String returnString = "[";
    TokenType firstTokenType = tokenList.getFirst().getType();

    for (TokenNode i : tokenList) {
      if (firstTokenType != i.getType()) {
        System.err.println("Error: mixed type list not supported.");
        return null;
      } else {
        returnString += i.evaluate(env) + ", ";
      }
    }

    returnString = returnString.substring(0, returnString.lastIndexOf(", "));
    return returnString += "]";
  }
}
