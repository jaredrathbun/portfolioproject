package lexer;

/** An enumeration of token types. */
public enum TokenType {
  /** An integer token. */
  INT,

  /** A real number token. */
  REAL,

  /** An identifier token. */
  ID,

  /** Add operation token. */
  ADD,

  /** Subtract operation token. */
  SUB,

  /** Multiply operation token. */
  MULT,

  /** Divide operation token. */
  DIV,

  /** A let token */
  LET,

  /** An in token. */
  IN,

  /** Assign operation. */
  ASSIGN,

  /** A left parenthesis. */
  LPAREN,

  /** A right parenthesis */
  RPAREN,

  /** An unknown token. */
  UNKNOWN,

  /** The end of the file token. */
  EOF,

  /** Identifier for a List. */
  LIST,

  /** Identifier for a HEAD operation. */
  HEAD,

  /** Identifier for a TAIL operation. */
  TAIL,

  /** 
   * Identifier for concatenation operation. 
   */
  CONCAT,

  /**
   * Identifier for a comma.
   */
  COMMA
}
