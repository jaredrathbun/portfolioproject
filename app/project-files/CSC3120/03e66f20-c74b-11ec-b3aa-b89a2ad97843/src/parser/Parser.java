package parser;

import ast.SyntaxTree;
import ast.nodes.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import lexer.Lexer;
import lexer.Token;
import lexer.TokenType;

/**
 * Implements a generic super class for parsing files.
 *
 * @author Zach Kissel
 * @author Jared Rathbun
 * @author Bernardo Santos
 */
public class Parser {
  private Lexer lex; // The lexer for the parser.
  private boolean errorFound; // True if ther was a parser error.
  private boolean doTracing; // True if we should run parser tracing.
  private Token nextTok; // The current token being analyzed.
  private String currentID;

  /**
   * Constructs a new parser for the file {@code source} by setting up lexer.
   *
   * @param src the source code file to parse.
   * @throws FileNotFoundException if the file can not be found.
   */
  public Parser(File src) throws FileNotFoundException {
    lex = new Lexer(src);
    errorFound = false;
    doTracing = false;
  }

  /**
   * Construct a parser that parses the string {@code str}.
   *
   * @param str the code to evaluate.
   */
  public Parser(String str) {
    lex = new Lexer(str);
    errorFound = false;
    doTracing = false;
  }

  /** Turns tracing on an off. */
  public void toggleTracing() {
    doTracing = !doTracing;
  }

  /**
   * Determines if the program has any errors that would prevent evaluation.
   *
   * @return true if the program has syntax errors; otherwise, false.
   */
  public boolean hasError() {
    return errorFound;
  }

  /**
   * Parses the file according to the grammar.
   *
   * @return the abstract syntax tree representing the parsed program.
   */
  public SyntaxTree parse() {
    SyntaxTree ast;

    nextToken(); // Get the first token.
    ast = new SyntaxTree(evalExpr()); // Start processing at the root of the tree.

    if (nextTok.getType() != TokenType.EOF)
      logError("Parse error, unexpected token " + nextTok);
    return ast;
  }

  /************
   * Private Methods.
   *
   * It is important to remember that all of our non-terminal processing methods
   * maintain the invariant that each method leaves the next unprocessed token
   * in {@code nextTok}. This means each method can assume the value of
   * {@code nextTok} has not yet been processed when the method begins.
   ***********/

  /**
   * Method to handle the expression non-terminal
   *
   * <p>
   * <expr> -> let <id> := <expr> in <expr> | <term> {(+ | - ) <term>}
   */
  private SyntaxNode evalExpr() {
    trace("Enter <expr>");
    SyntaxNode rterm;
    TokenType op;
    SyntaxNode expr = null;

    // Try to handle a let.
    if (nextTok.getType() == TokenType.LET) {
      nextToken();
      return handleLet();
    } else // Handle an addition or subtraction.
    {
      expr = evalTerm();

      while (nextTok.getType() == TokenType.ADD || nextTok.getType() == TokenType.SUB) {
        op = nextTok.getType();
        nextToken();
        rterm = evalTerm();
        expr = new BinOpNode(expr, op, rterm);
      }
    }

    trace("Exit <expr>");

    return expr;
  }

  /**
   * This method handles a let expression <id> := <expr> in <expr>
   *
   * @return a let node.
   */
  private SyntaxNode handleLet() {
    Token var = null;
    SyntaxNode varExpr;
    SyntaxNode expr;

    trace("enter handleLet");

    // Handle the identifier.
    if (nextTok.getType() == TokenType.ID) {
      var = nextTok;
      currentID = var.getValue();
      nextToken();

      // Handle the assignment.
      if (nextTok.getType() == TokenType.ASSIGN) {
        nextToken();
        varExpr = evalExpr();

        // Handle the in expr.
        if (nextTok.getType() == TokenType.IN) {
          nextToken();
          expr = evalExpr();
          return new LetNode(var, varExpr, expr);
        } else {
          logError("Let expression expected in, saw " + nextTok + ".");
        }
      } else {
        logError("Let expression missing assignment!");
      }

    } else
      logError("Let expression missing variable.");
    trace("exit handleLet");
    return null;
  }

  /**
   * Method to handle the term non-terminal.
   *
   * 
   * <term> → <factor> { ( * | / | ++ ) <factor> }
   */
  private SyntaxNode evalTerm() {
    SyntaxNode rfact;
    TokenType op;
    SyntaxNode term;

    trace("Enter <term>");
    term = evalFactor();

    while (nextTok.getType() == TokenType.MULT || nextTok.getType() == TokenType
        .DIV || nextTok.getType() == TokenType.CONCAT) {
      op = nextTok.getType();
      nextToken();
      rfact = evalFactor();
      term = new BinOpNode(term, op, rfact);
    }
    trace("Exit <term>");
    return term;
  }

  /**
   * Method to handle the factor non-terminal.
   *
   * <pre>
   * <factor> → <id> | <int> | <real> | ( <expr> ) | hd <factor> | tl <factor> |
   * list( [ (<id> |
   * <int> | <real>) {, (<id> | <int> | <real>)}] )
   * </pre>
   */
  private SyntaxNode evalFactor() {
    trace("Enter <factor>");
    SyntaxNode fact = null;
    switch (nextTok.getType()) {
      case LIST:
        LinkedList<TokenNode> listContents = new LinkedList<>();
        nextToken();

        // Swallow the left paren.
        nextToken();

        TokenType nextTokType = nextTok.getType();
        while (nextTokType == TokenType.INT || nextTokType == TokenType.REAL) {

          listContents.add(new TokenNode(nextTok));
          nextToken();
          if (nextTok.getType() == TokenType.COMMA) {
            nextToken();
          }
          nextTokType = nextTok.getType();
        }

        // Swallow the right paren.
        nextToken();

        // If the list is of size 0, nothing was in the parenthesis.
        if (listContents.size() != 0) {
          fact = new ListNode(listContents);
        }
        
        break;

      case HEAD:
        nextToken();
        fact = new HeadNode(evalFactor());
        break;

      case TAIL:
        nextToken();
        fact = new TailNode(evalFactor());
        break;

      case ID:
      case INT:
      case REAL:
        fact = new TokenNode(nextTok);
        nextToken();
        break;

      case LPAREN:
        nextToken();
        fact = evalExpr();

        if (nextTok.getType() == TokenType.RPAREN) {
          nextToken();
        } else {
          logError("Expected \")\" received " + nextTok + ".");
        }
        break;

      default:
        logError("Unexpected token " + nextTok);
        break;
    }

    trace("Exit <factor>");
    return fact;
  }

  /**
   * Logs an error to the console.
   *
   * @param msg the error message to dispaly.
   */
  private void logError(String msg) {
    System.err.println("Error (" + lex.getLineNumber() + "): " + msg);
    errorFound = true;
  }

  /**
   * This prints a message to the screen on if {@code doTracing} is true.
   *
   * @param msg the message to display to the screen.
   */
  private void trace(String msg) {
    if (doTracing)
      System.out.println(msg);
  }

  /**
   * Gets the next token from the lexer potentially logging that token to the
   * screen.
   */
  private void nextToken() {
    nextTok = lex.nextToken();

    if (doTracing)
      System.out.println("nextToken: " + nextTok);
  }
}
